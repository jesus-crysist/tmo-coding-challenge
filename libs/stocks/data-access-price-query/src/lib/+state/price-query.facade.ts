import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { map, skip, tap } from 'rxjs/operators';
import { FetchPriceQuery } from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import { getAllPriceQueries, getPriceQueryError, getSelectedSymbol } from './price-query.selectors';

@Injectable()
export class PriceQueryFacade {

  selectedSymbol$ = this.store.pipe(select(getSelectedSymbol));

  priceQueries$ = this.store.pipe(
    select(getAllPriceQueries),
    skip(1),
    map(priceQueries =>
      priceQueries.map(priceQuery => [priceQuery.date, priceQuery.close])
    )
  );

  errorMessage$ = this.store.pipe(
    select(getPriceQueryError),
    skip(1),
    tap(error => console.log(error))
  );

  constructor(private store: Store<PriceQueryPartialState>) {
  }

  fetchQuote(symbol: string, period: string) {
    this.store.dispatch(new FetchPriceQuery(symbol, period));
  }
}
