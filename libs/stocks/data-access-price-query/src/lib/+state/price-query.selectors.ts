import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PRICEQUERY_FEATURE_KEY, priceQueryAdapter, PriceQueryState } from './price-query.reducer';

const getPriceQueryState = createFeatureSelector<PriceQueryState>(
  PRICEQUERY_FEATURE_KEY
);

export const getSelectedSymbol = createSelector(
  getPriceQueryState,
  (state: PriceQueryState) => state.selectedSymbol
);

const {selectAll} = priceQueryAdapter.getSelectors();

export const getAllPriceQueries = createSelector(
  getPriceQueryState,
  selectAll
);

export const getPriceQueryError = createSelector(
  getPriceQueryState,
  (state: PriceQueryState) => state.error
);
